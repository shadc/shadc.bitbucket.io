define(['dojo/_base/declare',
  'jimu/BaseWidget',
  'dijit/_WidgetsInTemplateMixin',
  'dojo/_base/lang',
  'dojo/_base/html',
  'dojo/_base/array',
  'dojo/on',
  'dojo/promise/all',
  "esri/layers/GraphicsLayer",
  'esri/symbols/SimpleMarkerSymbol',
  'esri/symbols/SimpleLineSymbol',
  'esri/symbols/SimpleFillSymbol',
  "esri/graphic",
  "esri/tasks/query",
  "esri/graphicsUtils",
  'esri/symbols/jsonUtils',
  'esri/Color',
  'esri/geometry/geometryEngine',
  "esri/layers/FeatureLayer",
  "esri/tasks/RelationshipQuery",
  'jimu/utils',
  'jimu/dijit/FeatureSetChooserForMultipleLayers',
  'jimu/SelectionManager',
  'jimu/LayerInfos/LayerInfos',
  './layerUtil',
  'jimu/dijit/LoadingShelter'
],
  function (declare, BaseWidget, _WidgetsInTemplateMixin, lang, html, array, on, all, GraphicsLayer, SimpleMarkerSymbol,
    SimpleLineSymbol, SimpleFillSymbol, Graphic, Query, GraphicsUtils, SymbolJsonUtils, Color, GeometryEngine,
    FeatureLayer, RelationshipQuery, jimuUtils, FeatureSetChooserForMultipleLayers, SelectionManager, LayerInfos, layerUtil) {
    //To create a widget, you need to derive from BaseWidget.
    return declare([BaseWidget, _WidgetsInTemplateMixin], {
      // Custom widget code goes here

      baseClass: 'jimu-widget-AP-Address-Export',


      //methods to communication with app container:
      postCreate: function () {
        this.inherited(arguments);

        var selectionColor = new Color(this.config.selectionColor);
        this.defaultPointSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE,
          16, null, selectionColor);
        this.defaultLineSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
          selectionColor, 2);
        this.defaultFillSymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
          this.defaultLineSymbol,
          new Color([selectionColor.r, selectionColor.g, selectionColor.b, 0.3]));

        // create select dijit
        this.selectDijit = new FeatureSetChooserForMultipleLayers({
          map: this.map,
          updateSelection: true,
          fullyWithin: this.config.selectionMode === 'wholly',
          geoTypes: this.config.geometryTypes || ['EXTENT']
        });

        html.place(this.selectDijit.domNode, this.selectDijitNode);
        this.selectDijit.startup();

        this.own(on(this.selectDijit, 'user-clear', lang.hitch(this, this._clearAllSelections)));
        this.own(on(this.selectDijit, 'loading', lang.hitch(this, function () {
          this.shelter.show();
        })));

        this.own(on(this.selectDijit, 'unloading', lang.hitch(this, function () {
          this.shelter.hide();
        })));

        var layerInfosObject = LayerInfos.getInstanceSync();

        layerUtil.getLayerInfoArray(layerInfosObject).then(lang.hitch(this, function (layerInfoArray) {
          //First loaded, reset selectableLayerIds
          this._initLayers(layerInfoArray);
        }));

        var featurLayer = this.layerObjectArray[0];
        this.buffering = false;

        this.addressLink = document.createElement('a');
        this.addressLink.target = '_blank';

        this.own(on(this.bufferBtn, 'click', lang.hitch(this, this._bufferSelection)));
        this.own(on(this.addressBtn, 'click', lang.hitch(this, this._getAddresses)));

        this.own(on(this.chkSitus, 'change', lang.hitch(this, this._chkChanged)));
        this.own(on(this.chkMailing, 'change', lang.hitch(this, this._chkChanged)));
      },
       _chkChanged: function(event){
        if (event.target.id === "addressTypeSitus"){
          this.fileName.value= "SitusAddress";
        }else{
          this.fileName.value= "MailingAddress";
        }
       },
      startup: function () {
        this.inherited(arguments);
        //console.log('startup');
      },

      onOpen: function () {
        //console.log('onOpen');
      },

      onClose: function () {
        //console.log('onClose');
      },

    onActive: function(){
      if (!this.selectDijit.isActive()) {
        this.selectDijit.activate();
      }
    },

    onDeActive: function(){
      if (this.selectDijit.isActive()) {
        this.selectDijit.deactivate();
      }
    },
    
    onDestroy: function() {
      if (this.selectDijit.isActive()) {
        this.selectDijit.deactivate();
      }
      this._clearAllSelections();
    },

    _clearBufferSelections: function () {
      if (this.bufferGraphics) {
        var bufferGraphicsLayer = this.map.getLayer(this.bufferGraphics.id);
        if (bufferGraphicsLayer) {
          this.map.removeLayer(bufferGraphicsLayer);
          this.bufferGraphics = null;
          html.removeClass(this.addressBtn, 'button-enabled');
          html.addClass(this.addressBtn, 'button-disabled');
        }
      }
    },


      _clearAllSelections: function () {
        this._clearBufferSelections();
        this._clearfeatureSelections();

        html.removeClass(this.bufferBtn, 'button-enabled');
        html.addClass(this.bufferBtn, 'button-disabled');
      },

      _clearfeatureSelections: function () {
        var selectionMgr = SelectionManager.getInstance();
        array.forEach(this.layerObjectArray, function (layerObject) {
          selectionMgr.clearSelection(layerObject);
        });
      },


      _initLayers: function (layerInfoArray) {
        this.layerObjectArray = [];
        this.selectionSymbols = {};
        this.shelter.show();

        all(this._obtainLayerObjects(layerInfoArray)).then(lang.hitch(this, function (layerObjects) {
          array.forEach(layerObjects, lang.hitch(this, function (layerObject, index) {
            // hide from the layer list if layerobject is undefined or there is no objectIdField
            //Dial2_Taxlots - Taxlot
            if (layerObject && layerObject.objectIdField && layerObject.geometryType && layerObject.name == this.config.selectionLayer) {
              this.layerObjectArray.push(layerObject);
              if (!layerObject.getSelectionSymbol()) {
                this._setDefaultSymbol(layerObject);
              }
              var symbol = layerObject.getSelectionSymbol();
              this.selectionSymbols[layerObject.id] = symbol.toJson();
              this.own(on(layerObject, 'selection-complete', lang.hitch(this, function () {
                if (!this.buffering) {
                  this._clearBufferSelections();
                }
                html.removeClass(this.bufferBtn, 'button-disabled');
                html.addClass(this.bufferBtn, 'button-enabled');
                this.buffering = false;
                this.shelter.hide();
              })));
            }
          }));
          this.selectDijit.setFeatureLayers(this.layerObjectArray);
          this._setSelectionSymbol();
          this.shelter.hide();


        }));
      },

      _obtainLayerObjects: function (layerInfoArray) {
        return array.map(layerInfoArray, function (layerInfo) {
          return layerInfo.getLayerObject();
        });
      },

      _setDefaultSymbol: function (layerObject) {
        if (layerObject.geometryType === 'esriGeometryPoint' ||
          layerObject.geometryType === 'esriGeometryMultipoint') {
          layerObject.setSelectionSymbol(this.defaultPointSymbol);
        } else if (layerObject.geometryType === 'esriGeometryPolyline') {
          layerObject.setSelectionSymbol(this.defaultLineSymbol);
        } else if (layerObject.geometryType === 'esriGeometryPolygon') {
          layerObject.setSelectionSymbol(this.defaultFillSymbol);
        } else {
          console.warn('unknown geometryType: ' + layerObject.geometryType);
        }
      },

      _setSelectionSymbol: function () {
        array.forEach(this.layerObjectArray, function (layerObject) {
          this._setDefaultSymbol(layerObject);
        }, this);
      },

      _bufferSelection: function (event) {
        var featurLayer = this.layerObjectArray[0];
        var graphics = featurLayer.getSelectedFeatures();
        if (graphics.length > 0) {
          this.buffering = true;
          this.shelter.show();
          var sfsOrginalSelection = new SimpleFillSymbol(SimpleFillSymbol.STYLE_NULL,
            new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
              new Color([255, 255, 153]), 2), null);
          var sfsBuffer = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
            new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
              new Color([255, 0, 0]), 4), new Color([255, 0, 0, .25]));

          //-- If there is already a buffer, then use the original buffer selection stored in the graphic layer.
          if (this.bufferGraphics) {
            var bufferGraphics = this.bufferGraphics.graphics;
            if (bufferGraphics.length > 0) {
              graphics = bufferGraphics.slice(1);
              //graphics = bufferGraphics.slice(-1)[0];


              this._clearBufferSelections();
            }
          }

          var selectedFeatureSet = jimuUtils.toFeatureSet(graphics);
          var geomArr = GraphicsUtils.getGeometries(graphics);
          var buff = GeometryEngine.buffer(geomArr, this.bufferDist.value, "feet", true)[0];
          var selectQuery = new Query();
          selectQuery.geometry = buff;
          featurLayer.selectFeatures(selectQuery, FeatureLayer.SELECTION_NEW);

          this.bufferGraphics = new GraphicsLayer();
          this.bufferGraphics.add(new Graphic(buff, sfsBuffer));

          for (var i = 0, il = graphics.length; i < il; i++) {
            var graphic = new Graphic(graphics[i].toJson());
            graphic.setSymbol(sfsOrginalSelection);
            this.bufferGraphics.add(graphic);
          }



          this.map.addLayer(this.bufferGraphics);

          html.removeClass(this.addressBtn, 'button-disabled');
          html.addClass(this.addressBtn, 'button-enabled');

        }
      },



      _getAddresses: function (event) {

        if (!this.bufferGraphics) return;

        var featurLayer = this.layerObjectArray[0];
        var selected = featurLayer.getSelectedFeatures();
        var situs = document.getElementById("addressTypeSitus");

        //For AP
        oids = [];
        for (var i = 0, il = selected.length; i < il; i++) {
          oids.push(selected[i].attributes.OBJECTID);
        }
        var relatedTopsQuery = new RelationshipQuery();
        relatedTopsQuery.outFields = ["*"];
        //relate ids are 1= mailing, 2 = situs 
        relatedTopsQuery.relationshipId = (situs.checked) ? 2 : 1;
        relatedTopsQuery.objectIds = oids;

        featurLayer.queryRelatedFeatures(relatedTopsQuery).then(lang.hitch(this, function (relatedRecords) {
          var A = [];
          if (situs.checked) {
            A.push(['Address', 'Address2', 'City', 'State']);
          }else{
            A.push(['Name', 'Address', 'Address2', 'City', 'State', 'Zip']);
          }

          for (var r in relatedRecords) {
            var fset = relatedRecords[r];
            console.log(fset);
            if (fset != undefined) {
              dojo.map(fset.features, function (feature) {
                let atts = feature.attributes;
                if (situs.checked) {
                  let add = atts.TNUM + " " + atts.TDIR + " " + atts.TROAD + " " + atts.TLANE;
                  add = add.replace(/ +/g, " ").trim();
                  let add2 = '';
                  let city = atts.TCITY;
                  let state = 'OR';
                  let val = [add, add2, city, state];
                   if (add.length > 1&& array.indexOf(A, val) == -1){
                    A.push(val);
                  }
                } else {
                  let name = atts.MNAM.replace(",", "^");
                  let add = atts.MAD1.trim();
                  let add2 = atts.MAD2;
                  let city = atts.MCST.substr(0, atts.MCST.length - 3);
                  let state = atts.MCST.slice(-2);
                  let zip = atts.MZIP;
                  let val = ['"' + name + '"', add, add2, city, state, zip];
                  if (add.length > 1 && array.indexOf(A, val) == -1){
                    A.push(val);
                  }
                }
              });
            }
          }

          var csvRows = [];
          for (var i = 0, l = A.length; i < l; ++i) {
            csvRows.push(A[i].join(',').replace("^", ","));
          }
          // //var csvString = csvRows.join("%0A");
          var csvString = csvRows.join("\r\n");

          if (window.navigator.msSaveOrOpenBlob) {
            var blob = new Blob([csvString]);
            if (this.fileName.value.trim().length > 0) {
              window.navigator.msSaveOrOpenBlob(blob, this.fileName.value.trim() + '.csv');
            }else{
              window.navigator.msSaveOrOpenBlob(blob, 'Addresses.csv');
            }
          } else {
            if (this.fileName.value.trim().length > 0) {
              this.addressLink.download = this.fileName.value.trim() + '.csv';
            }else{
              this.addressLink.download = 'Addresses.csv';
            }
            this.addressLink.href = 'data:attachment/csv,' + encodeURIComponent(csvString);
            this.addressLink.click();
          }
        }));


        // //--For Deschutes
        // var A = [['Address', 'Address2', 'City', 'State', 'Zip']];
        // for (var i = 0, il = selected.length; i < il; i++) {
        //   if (situs.checked) {
        //     let add = selected[i].attributes['Taxlot_Assessor_Account.Address'];
        //     let add2 = '';
        //     let city = selected[i].attributes['Taxlot_Assessor_Account.City'];
        //     let state = 'OR';
        //     let zip = selected[i].attributes['Taxlot_Assessor_Account.Zip']
        //     A.push([add, add2, city, state, zip]);
        //   } else {
        //     let add = selected[i].attributes['dbo_GIS_MAILING.M_ADDRESS'];
        //     let add2 = '';
        //     let city = selected[i].attributes['dbo_GIS_MAILING.M_CITY'];
        //     let state = selected[i].attributes['dbo_GIS_MAILING.M_STATE']
        //     let zip = selected[i].attributes['dbo_GIS_MAILING.M_ZIP']
        //     A.push([add, add2, city, state, zip]);
        //   }
        // }

        // var csvRows = [];
        // for (var i = 0, l = A.length; i < l; ++i) {
        //   console.log(A[i]);
        //   csvRows.push(A[i].join(','));
        // }

        // // var csvString = csvRows.join("%0A");
        // var csvString = csvRows.join("\r\n");
        // if (window.navigator.msSaveOrOpenBlob) {
        //   var blob = new Blob([csvString]);
        //   window.navigator.msSaveOrOpenBlob(blob, 'myFile.csv');
        // } else {
        //   this.addressLink.href = 'data:attachment/csv,' + encodeURIComponent(csvString);
        //   this.addressLink.click();
        // }

      }
    });
  });